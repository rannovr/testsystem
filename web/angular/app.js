'use strict';

angular.module('Libraries', ['angular-sha1', 'ui.router', 'utils', 'ngCookies', 'ngDialog', 'ui.validate','ui.tree', 'smart-table']);

angular.module('Login', []);
angular.module('Dashboard', []);
angular.module('UserDashboard', []);
angular.module('Registration', []);
angular.module('UserProfile', []);
angular.module('HistoryExams', []);

var app = angular.module('main', ['Login', 'Dashboard', 'UserProfile', 'Registration', 'Libraries', 'UserDashboard', 'HistoryExams'])
    .constant("constants", {rememberMeName: "rememberMeTestSystem", eMail: "eMail"})
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.when('', '/login').when('/', '/login');

        $stateProvider.state('login', {
            url: '/login',
            controller: 'LoginController',
            templateUrl: '/angular/templates/states/login.html'
        })
            .state('dashboard', {
                url: '/dashboard',
                controller: 'DashboardController',
                templateUrl: '/angular/templates/states/dashboard.html'
            })
            .state('userdashboard', {
                url: '/userdashboard',
                controller: 'UserDashboardController',
                templateUrl: '/angular/templates/states/userdashboard.html'
            })
            .state('userprofile', {
                url: '/userprofile',
                controller: 'UserProfileController',
                templateUrl: '/angular/templates/states/userprofile.html'
            })
            .state('registration', {
                url: '/registration',
                controller: 'RegistrationController',
                templateUrl: '/angular/templates/states/registration.html'
            })
            .state('historyexams', {
                url: '/historyexams',
                controller: 'HistoryExamsController',
                templateUrl: '/angular/templates/states/historyexams.html'
            });

        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    }).run(function ($rootScope, utils) {
    });
