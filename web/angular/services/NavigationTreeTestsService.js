'use strict';

app.factory('NavigationTreeTestsService', function (Network) {
    return {
        treeCategories: function (onSuccess, onError) {
            Network.sendGet("/services/categories/treeCategories", null, onSuccess, onError);
        },
        tests: function (scope, onSuccess, onError) {
            Network.sendGet("/services/tests/search", {categoryID: scope.$modelValue.id}, onSuccess, onError);
        }
    };
});