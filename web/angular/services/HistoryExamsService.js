'use strict';

app.factory('HistoryExamsService', function (Network, $cookies, constants) {
    return {
        showHistoryExams: function (onSuccess, onError) {
            Network.sendGet("/services/exams/search", {eMail: $cookies.getObject(constants.eMail)}, onSuccess, onError);
        }
    };
});
