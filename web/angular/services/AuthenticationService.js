'use strict';

app.factory('AuthenticationService', function ($state, $cookies, Network, constants) {
    return {
        login: function (credentials, onSuccess, onError) {
            var headers = credentials ? {authorization: "Basic " + btoa(credentials.username + ":" + credentials.password)} : {};
            Network.sendGetHeaders('/services/authentication/login', {}, headers, onSuccess, onError);
        },

        logout: function (onSuccess, onError) {
            $cookies.remove(constants.rememberMeName);
            $cookies.remove(constants.eMail);
            Network.sendGet("/services/authentication/logout", {}, onSuccess, onError);
        }
    };
});