'use strict';

app.factory('UsersService', function (Network) {
    return {
        getProfile: function(onSuccess, onError) {
            Network.sendGet("/services/users/getCurrentProfile", {}, onSuccess, onError);
        },
        changeProfile: function (name, surname, onSuccess, onError) {
            Network.sendPost("/services/users/changeCurrentProfile", {
                "name": name,
                "surname": surname
            }, onSuccess, onError);
        },
        changePassword: function(changePasswordData, onSuccess, onError) {
            Network.sendPost("/services/users/changePassword", changePasswordData, onSuccess, onError);
        }
    };
});