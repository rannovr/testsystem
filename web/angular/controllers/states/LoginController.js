'use strict';

angular.module('Login').controller('LoginController', function ($window, $scope, $state, $cookies, AuthenticationService, DialogManager, constants) {
    function setCookieEmail(eMail) {
        var now = new $window.Date(),
            exp = new $window.Date(now.getFullYear(), now.getMonth() + 1, now.getDate());
        $cookies.putObject(constants.eMail, eMail, {
            expires: exp
        });
    }

    function chooseStateByRole(response) {
        switch (response.userEntity.role) {
            case "ROLE_ADMIN":
                return $state.go("dashboard");
            case "ROLE_OPERATOR":
                return $state.go("userdashboard");
            case "ROLE_TEACHER":
                return $state.go("userdashboard");
            case "ROLE_USER":
                return $state.go("dashboard");
            default :
                return $state.go("login");
        }
    }

    var rememberMeCredentials = $cookies.getObject(constants.rememberMeName);
    if (rememberMeCredentials) {
        AuthenticationService.login(rememberMeCredentials, function onSuccess(response) {
                chooseStateByRole(response);
            },
            function onError() {
                $cookies.remove(constants.rememberMeName);
            }
        );
    }

    $scope.login = function () {
        AuthenticationService.login($scope.credentials, function onSuccess(response) {
                setCookieEmail(response.userEntity.eMail);
                if ($scope.rememberMe === true) {
                    var now = new $window.Date(),
                        exp = new $window.Date(now.getFullYear(), now.getMonth() + 1, now.getDate());
                    $cookies.putObject(constants.rememberMeName, $scope.credentials, {
                        expires: exp
                    });
                } else {
                    $cookies.remove(constants.rememberMeName);
                }
                chooseStateByRole(response);
            },
            function onError(message) {
                DialogManager.showInformation(message);
                $state.go("login");
            }
        );
    };
    
    $scope.register = function () {
        $state.go("registration");
    }
});