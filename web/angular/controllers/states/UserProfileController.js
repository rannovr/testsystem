'use strict';

angular.module('UserProfile').controller('UserProfileController', function ($scope, $state, $cookies, UsersService, DialogManager) {
    $scope.user = {};
    $scope.userRoles = { ROLE_USER: 'User', ROLE_TEACHER: 'Teacher', ROLE_OPERATOR: 'Operator', ROLE_ADMIN: 'Administrator' };

    $scope.getProfile = function () {
        $scope.profile = UsersService.getProfile(
            function onSuccess(payload) {
                $scope.user.email = payload.eMail;
                $scope.user.name = payload.name;
                $scope.user.surname = payload.surname;
                $scope.user.role = $scope.userRoles[payload.role];
            },
            function onError() {
                $state.go("login");
            });
    };

    $scope.changePassword = function () {
        UsersService.changePassword(
            {newPassword: $scope.newPassword, oldPassword: $scope.oldPassword}, function onSuccess(payload) {
                DialogManager.showInformation(payload.message);
            }, function onError(message) {
                DialogManager.showInformation(message);
            }
        );
    };

    $scope.saveChanges = function () {
        UsersService.changeProfile($scope.user.name, $scope.user.surname,
            function onSuccess(payload) {
                DialogManager.showInformation(payload.message);
            },
            function onError(message) {
                DialogManager.showInformation(message);
            });
    };

});