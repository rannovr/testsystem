'use strict';

angular.module('Dashboard').controller('DashboardController', function ($scope, $state, $cookies, DialogManager, AuthenticationService, UsersService, NavigationTreeTestsService) {
    $scope.user = {};
    $scope.profile = UsersService.getProfile(
        function onSuccess(payload) {
            $scope.user.name = payload.name;
            $scope.user.surname = payload.surname;
        },
        function onError() {
        });

    $scope.toProfile = function () {
        $state.go("userprofile");
    };

    $scope.logout = function() {
        AuthenticationService.logout(
            function onSuccess() {
                console.log("logout was successful");
                $state.go("login");
            }, function onError(){
                console.log("logout was with error");
                $state.go("login");
            }
        )
    };

    $scope.show_history = function() {
        $state.go("historyexams");
    };

    NavigationTreeTestsService.treeCategories(function onSuccess(payload) {
        $scope.treeData = payload;
    }, function onError() {
        console.log("Some problem");
    });

    $scope.rowCollection = [];
    $scope.getTests = function (scope) {
        if (scope.$modelValue.isLeaf) {
            NavigationTreeTestsService.tests(scope, function onSuccess(payload) {
                $scope.rowCollection = payload;
            })
        }
    };
});