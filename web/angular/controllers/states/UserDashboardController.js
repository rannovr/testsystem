'use strict';

angular.module('UserDashboard').controller('UserDashboardController', function ($scope, $state, AuthenticationService) {

    $scope.logout = function() {
        AuthenticationService.logout(
            function onSuccess() {
                console.log("logout was successful");
                $state.go("login");
            }, function onError(){
                console.log("logout was with error");
                $state.go("login");
            }
        )
    }
});