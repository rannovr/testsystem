'use strict';

angular.module('HistoryExams').controller('HistoryExamsController', function ($scope, $state, HistoryExamsService) {
    $scope.rowCollection = [];

    HistoryExamsService.showHistoryExams(function onSuccess(payload) {
        $scope.rowCollection = payload;
    },
    function onError(response) {
        console.log(response);
    });

    $scope.godashbord = function () {
        $state.go('dashboard');
    }
});
