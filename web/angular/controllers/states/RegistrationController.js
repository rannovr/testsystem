'use strict';

angular.module('Registration').controller('RegistrationController', function ($scope, $state, RegistrationService, DialogManager) {
    $scope.sighUp = function () {
        RegistrationService.signUp($scope.registrationData,
            function onSuccess(payload) {
                DialogManager.showInformation(payload.message);
                if (payload.isRegistered)
                    $state.go("login");
            },
            function onError() {
                $state.go("registration");
            }
        );
    };

    $scope.backToLogin = function () {
        $state.go("login");
    }
});