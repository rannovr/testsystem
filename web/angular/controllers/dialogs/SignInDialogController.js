app.controller('SignInDialogController', function($scope, $state, AuthenticationService, DialogManager) {
    $scope.signIn = function() {
        AuthenticationService.signIn($scope.userName, $scope.password,
            function onSuccess(payload) {
                $scope.closeThisDialog();
                $state.go('dashboard');
            },
            function onError(message) {
                DialogManager.showInformation(message);
            });
    };
});