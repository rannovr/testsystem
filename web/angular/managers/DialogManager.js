'use strict';

app.factory('DialogManager', function (ngDialog) {
    return {
        /* Use this method to open dialog
        *  example DialogManager.show("signInDialog.html", "SignInDialogController");
        *  */
        show: function(templateName, controllerName, parameters) {
            var dialogParameters = angular.extend({
                templateUrl : "/angular/templates/dialogs/" + templateName,
                controller :  controllerName
            }, parameters);

            ngDialog.open(dialogParameters);
        },

        showInformation: function(text) {
            this.show('informationDialog.html', function($scope) {
                $scope.text = text;
            }, {showClose: false});
        },

        showConfirmation: function(text, onOk, onCancel) {
            this.show('confirmationDialog.html', function($scope) {
                $scope.text = text;
                $scope.yes = function() { return onOk($scope) };
                $scope.no  = function() { return onCancel($scope) };
            });
        }
    };
});
