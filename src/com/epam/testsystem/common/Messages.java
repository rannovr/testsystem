package com.epam.testsystem.common;

public final class Messages {
    /* Change Password */
    public final static String PASSWORD_CHANGED             = "Password have been changed successfully";
    public final static String PASSWORD_DONOT_CHANGED       = "Password haven't been changed!";

    /* Change Profile */
    public final static String PROFILE_CHANGED              = "Profile has been changed successfully";
    public final static String PROFILE_DONOT_CHANGED        = "Profile hasn't been changed!";

    /* Registration */
    public final static String REGISTRATION_SUCCESSFUL      = "Registration successful!";
    public final static String REGISTRATION_UNSUCCESSFUL    = "Registration unsuccessful!";

    /* Authentication */
    public final static String AUTHENTICATION_SUCCESSFUL    =  "Welcome, ";
    public final static String AUTHENTICATION_UNSUCCESSFUL  =  "Username or password incorrect, please try again.";
}
