package com.epam.testsystem.common;

public final class Parameters {
    public final static String USER_ID          = "userID";
    public final static String EMAIL            = "eMail";
    public final static String PARENT_ID        = "parentID";
    public final static String CATEGORY_ID      = "categoryID";
    public final static String TEST_ID          = "testID";
    public final static String STATUS           = "status";
}