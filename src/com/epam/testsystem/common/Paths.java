package com.epam.testsystem.common;

public final class Paths {
    private final static String SERVICES = "/services";

    public final static String USERS            = SERVICES + Services.USERS;
    public final static String TESTS            = SERVICES + Services.TESTS;
    public final static String EXAMS            = SERVICES + Services.EXAMS;
    public final static String CATEGORIES       = SERVICES + Services.CATEGORIES;
    public final static String AUTHENTICATION   = SERVICES + Services.AUTHENTICATION;
}
