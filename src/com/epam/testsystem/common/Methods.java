package com.epam.testsystem.common;

public final class Methods {
    /* Common */
    public final static String GET                  = "/get";
    public final static String CREATE               = "/create";
    public final static String SEARCH               = "/search";

    /* User methods */
    public final static String GET_CURRENT_PROFILE  = "/getCurrentProfile";
    public final static String GET_PROFILE          = "/getProfile";
    public final static String CHANGE_PASSWORD      = "/changePassword";
    public final static String CHANGE_CURRENT_PROFILE       = "/changeCurrentProfile";

    /*Category methods*/
    public final static String TREE_CATEGORIES      = "/treeCategories";
    /* Authentication */
    public final static String LOGIN                = "/login";
    public final static String LOGOUT               = "/logout";
}
