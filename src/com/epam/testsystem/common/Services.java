package com.epam.testsystem.common;

public final class Services {
    public static final String USERS            = "/users";
    public static final String TESTS            = "/tests";
    public static final String CATEGORIES       = "/categories";
    public static final String EXAMS            = "/exams";
    public static final String AUTHENTICATION   = "/authentication";
}
