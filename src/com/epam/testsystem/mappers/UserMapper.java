package com.epam.testsystem.mappers;

import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.tables.UsersTable;

import java.sql.SQLException;

public final class UserMapper extends AbstractMapper<UserEntity> {
    @Override
    public final UserEntity map(final Row row) throws SQLException {
        final UserEntity userEntity = new UserEntity();
        userEntity.eMail = row.getString(UsersTable.USER_EMAIL);
        userEntity.password = row.getString(UsersTable.USER_PASS);
        userEntity.name = row.getString(UsersTable.USER_NAME);
        userEntity.surname = row.getString(UsersTable.USER_SURNAME);
        userEntity.role = row.getString(UsersTable.USER_ROLE);
        return userEntity;
    }
}
