package com.epam.testsystem.mappers;

import com.epam.testsystem.entities.TestEntity;
import com.epam.testsystem.tables.TestsTable;

import java.sql.SQLException;

public class TestMapper extends AbstractMapper<TestEntity> {
    @Override
    public final TestEntity map(final Row row) throws SQLException {
        final TestEntity testEntity = new TestEntity();
        testEntity.id = row.getLong(TestsTable.TEST_ID);
        testEntity.testName = row.getString(TestsTable.TEST_NAME);
        testEntity.allocateTime = row.getTime(TestsTable.ALLOCATED_TIME);
        testEntity.categoryID = row.getLong(TestsTable.CATEGORY_ID);
        testEntity.isAvailable = row.getBoolean(TestsTable.IS_AVAILABLE);
        return testEntity;
    }
}
