package com.epam.testsystem.mappers;

import com.epam.testsystem.entities.ExamEntity;
import com.epam.testsystem.tables.ExamsTable;

import java.sql.SQLException;

public final class ExamMapper extends AbstractMapper<ExamEntity> {
    @Override
    public final ExamEntity map(final Row row) throws SQLException {
        final ExamEntity examEntity = new ExamEntity();
        examEntity.id = row.getLong(ExamsTable.EXAM_ID);
        examEntity.userID = row.getLong(ExamsTable.EXAM_USER_ID);
        examEntity.testID = row.getLong(ExamsTable.EXAM_TEST_ID);
        examEntity.description = row.getString(ExamsTable.EXAM_DESCRIPTION);
        examEntity.date = row.getDate(ExamsTable.EXAM_DATE);
        examEntity.status = row.getString(ExamsTable.EXAM_STATUS);
        return examEntity;
    }
}
