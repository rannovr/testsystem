package com.epam.testsystem.mappers;

import com.epam.testsystem.entities.AbstractEntity;
import com.epam.testsystem.tables.AbstractTable;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Date;

public abstract class AbstractMapper<E extends AbstractEntity> implements RowMapper<E> {
    public final class Row {
        private final ResultSet result;

        public Row(final ResultSet result) {
            this.result = result;
        }

        public final Long getLong(final AbstractTable column) throws SQLException {
            return result.getLong(column.getColumnName());
        }

        public final Integer getInteger(final AbstractTable column) throws SQLException {
            return result.getInt(column.getColumnName());
        }

        public final String getString(final AbstractTable column) throws SQLException {
            return result.getString(column.getColumnName());
        }

        public final Date getDate(final AbstractTable column) throws SQLException {
            return result.getDate(column.getColumnName());
        }

        public final Time getTime(final AbstractTable column) throws  SQLException {
            return result.getTime(column.getColumnName());
        }

        public final boolean getBoolean(final AbstractTable column) throws SQLException {
            return result.getBoolean(column.getColumnName());
        }

        public final <T extends Enum<T>> T getEnum(final AbstractTable column, final Class<T> clazz) throws SQLException {
            return Enum.valueOf(clazz, getString(column));
        }

        public final <T> T getObject(final AbstractTable column, final Class<T> clazz) throws SQLException {
            return clazz.cast(result.getObject(column.getColumnName()));
        }
    }

    @Override
    public final E mapRow(final ResultSet resultSet, final int i) throws SQLException {
        return map(new Row(resultSet));
    }

    public abstract E map(final Row row) throws SQLException;
}
