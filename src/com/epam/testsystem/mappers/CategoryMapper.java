package com.epam.testsystem.mappers;


import com.epam.testsystem.entities.CategoryEntity;
import com.epam.testsystem.tables.CategoriesTable;

import java.sql.SQLException;

public class CategoryMapper extends AbstractMapper<CategoryEntity> {
    @Override
    public final CategoryEntity map(final Row row) throws SQLException {
        final CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.id = row.getLong(CategoriesTable.CATEGORY_ID);
        categoryEntity.categoryName = row.getString(CategoriesTable.CATEGORY_NAME);
        categoryEntity.parentID = row.getLong(CategoriesTable.PARENT_ID);
        return categoryEntity;
    }
}
