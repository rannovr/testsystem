package com.epam.testsystem.special;

import com.epam.testsystem.entities.AbstractEntity;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;

public final class Cache<E extends AbstractEntity> {
    public final ConcurrentHashMap<Long, E> id2Entity;

    public Cache(final Collection<E> entities) {
        id2Entity = new ConcurrentHashMap<>();

        entities.forEach(e -> id2Entity.put(e.id, e));
    }

    public final int getSize() {
        return id2Entity.size();
    }

    public final List<E> list() {
        return new ArrayList<>(id2Entity.values());
    }

    public final E getByID(final Long id) {
        return id2Entity.get(id);
    }

    public final boolean hasEntityWithID(final Long id) {
        return id2Entity.containsKey(id);
    }
}
