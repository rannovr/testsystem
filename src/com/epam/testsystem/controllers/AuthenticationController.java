package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.responses.AuthenticationResponse;
import com.epam.testsystem.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@RequestMapping(value = Paths.AUTHENTICATION)
public final class AuthenticationController extends AbstractController<UserEntity> {

    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = Methods.LOGIN, method = RequestMethod.GET)
    public final Result<AuthenticationResponse> login(final Principal principal){
        return run(() -> authenticationService.authenticate(principal.getName()));
    }

    @RequestMapping(value = Methods.LOGOUT, method = RequestMethod.GET)
    public final Result<Boolean> logout(final HttpServletRequest request, final HttpServletResponse response) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
            return run(() -> true);
        }
        return run(() -> false);
    }
}
