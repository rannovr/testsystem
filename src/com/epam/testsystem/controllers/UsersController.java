package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.ChangePasswordEntity;
import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.responses.RegistrationResponse;
import com.epam.testsystem.services.UsersService;
import com.epam.testsystem.responses.PasswordChangeResponse;
import com.epam.testsystem.responses.ProfileChangeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = Paths.USERS)
public class UsersController extends AbstractController<UserEntity> {
    @Autowired
    private UsersService usersService;

    @RequestMapping(value = Methods.GET, method = RequestMethod.GET)
    public final Result<UserEntity> getByID(@RequestParam(value = Parameters.USER_ID) final Long userID) {
        return run(() -> usersService.getUser(userID));
    }

    @RequestMapping(value = Methods.CREATE, method = RequestMethod.POST)
    public final Result<RegistrationResponse> create(@RequestBody final UserEntity userEntity) {
        return run(() -> usersService.createUser(userEntity.eMail, userEntity.password, userEntity.name, userEntity.surname));
    }

    @RequestMapping(value = Methods.CHANGE_PASSWORD, method = RequestMethod.POST)
    public final Result<PasswordChangeResponse> changePassword(@RequestBody final ChangePasswordEntity changePasswordEntity) {
        return run(() -> usersService.changeUserPassword(changePasswordEntity.newPassword, changePasswordEntity.oldPassword));
    }

/*    @RequestMapping(value = Methods.CHANGE_PROFILE, method = RequestMethod.POST)
    public final Result<ProfileChangeResponse> changeProfile(@RequestBody final UserEntity userEntity) {
        return run(() -> usersService.changeProfile(userEntity.id, userEntity.name, userEntity.surname));
    }*/

    @RequestMapping(value = Methods.GET_CURRENT_PROFILE, method = RequestMethod.GET)
    public final Result<UserEntity> getCurrentProfile() {
        return run(usersService::getCurrentProfile);
    }

    @RequestMapping(value = Methods.GET_PROFILE, method = RequestMethod.GET)
    public final Result<UserEntity> getProfile(@RequestParam(value = Parameters.USER_ID) final Long userID) {
        return run(() -> usersService.getProfile(userID));
    }

    @RequestMapping(value = Methods.CHANGE_CURRENT_PROFILE, method = RequestMethod.POST)
    public final Result<ProfileChangeResponse> changeCurrentProfile(@RequestBody final UserEntity userEntity) {
        return run(() -> usersService.changeCurrentProfile(userEntity.name, userEntity.surname));
    }
}
