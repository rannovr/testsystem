package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.TestEntity;
import com.epam.testsystem.services.TestsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = Paths.TESTS)
public class TestsController extends AbstractController<TestEntity> {
    @Autowired
    private TestsService testsService;

    @RequestMapping(value = Methods.SEARCH, method = RequestMethod.GET)
    public final Result<List<TestEntity>> getTestsByCategoryID(@RequestParam(value = Parameters.CATEGORY_ID) final Long categoryID) {
        return run(() -> testsService.getTestsByCategoryID(categoryID));
    }

    @RequestMapping(value = Methods.GET, method = RequestMethod.GET)
    public final Result<TestEntity> getTestByID(@RequestParam(value = Parameters.TEST_ID) final Long testID) {
        return run(() -> testsService.getByID(testID));
    }
}
