package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.ExamEntity;
import com.epam.testsystem.enums.ExamStatus;
import com.epam.testsystem.services.ExamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = Paths.EXAMS)
public class ExamsController extends AbstractController<ExamEntity> {
    @Autowired
    private ExamsService examsService;

    @RequestMapping(value = Methods.SEARCH, method = RequestMethod.GET)
    public final Result<List<ExamEntity>> searchByEmailAndStatus(@RequestParam(value = Parameters.EMAIL) final String eMail, @RequestParam(value = Parameters.STATUS, required = false) final ExamStatus status) {
        return run(() -> examsService.searchExamByEmailAndStatus(eMail, status));
    }
}
