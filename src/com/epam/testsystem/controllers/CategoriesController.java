package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.CategoryEntity;
import com.epam.testsystem.services.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = Paths.CATEGORIES)
public class CategoriesController extends AbstractController<CategoryEntity> {
    @Autowired
    private CategoriesService categoriesService;

    @RequestMapping(value = Methods.SEARCH, method = RequestMethod.GET)
    public final Result<List<CategoryEntity>> getChildrenByParentID(@RequestParam(value = Parameters.PARENT_ID) final Long parentID) {
        return run(() -> categoriesService.getChildrenByParentID(parentID));
    }

    @RequestMapping(value = Methods.TREE_CATEGORIES, method = RequestMethod.GET)
    public final Result<List<CategoryEntity>> testCategories() {
        return run(categoriesService::getFullTreeCategories);
    }
}
