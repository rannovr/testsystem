package com.epam.testsystem.tables;

public enum TestsTable implements AbstractTable {
    TEST_ID("testID"),
    TEST_NAME("testName"),
    IS_AVAILABLE("isAvailable"),
    CATEGORY_ID("categoryID"),
    ALLOCATED_TIME("allocatedTime");

    private final String columnName;

    TestsTable(final String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String getColumnName() { return this.columnName; }
}
