package com.epam.testsystem.tables;

public enum ExamsTable implements AbstractTable {
    EXAM_ID("examID"),
    EXAM_USER_ID("userID"),
    EXAM_TEST_ID("testID"),
    EXAM_DESCRIPTION("description"),
    EXAM_DATE("date"),
    EXAM_STATUS("status");

    private final String columnName;

    ExamsTable(final String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String getColumnName() {
        return columnName;
    }
}
