package com.epam.testsystem.tables;

public enum UsersTable implements AbstractTable {
    USER_ID("userID"),
    USER_PASS("password"),
    USER_EMAIL("eMail"),
    USER_NAME("name"),
    USER_SURNAME("surname"),
    USER_ROLE("role");

    private final String columnName;

    UsersTable(final String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String getColumnName() {
        return columnName;
    }
}
