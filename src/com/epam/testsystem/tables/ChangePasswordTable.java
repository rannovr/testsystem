package com.epam.testsystem.tables;

public enum ChangePasswordTable implements AbstractTable {
    USER_EMAIL("eMail"),
    USER_NEW_PASSWORD("newPassword"),
    USER_OLD_PASSWORD("oldPassword");

    private final String columnName;

    ChangePasswordTable(final String columnName) {
        this.columnName = columnName;
    }

    @Override
    public String getColumnName() {
        return columnName;
    }
}
