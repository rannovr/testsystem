package com.epam.testsystem.tables;

public enum CategoriesTable implements AbstractTable  {
    CATEGORY_ID("categoryID"),
    CATEGORY_NAME("categoryName"),
    PARENT_ID("parentID");

    private final String columnName;

    CategoriesTable(final String columnName) { this.columnName = columnName; }

    @Override
    public String getColumnName() {
        return columnName;
    }
}
