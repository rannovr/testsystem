package com.epam.testsystem.responses;

import com.epam.testsystem.common.Messages;
import com.epam.testsystem.entities.UserEntity;

public final class AuthenticationResponse {
    public final boolean isAuthenticated;
    public final String message;
    public UserEntity userEntity;

    public AuthenticationResponse() {
        this(false, Messages.AUTHENTICATION_UNSUCCESSFUL);
    }

    public AuthenticationResponse(final boolean isAuthenticated, final String message) {
        this.isAuthenticated = isAuthenticated;
        this.message         = message;
    }

    public AuthenticationResponse(final UserEntity userEntity) {
        this(true, Messages.AUTHENTICATION_SUCCESSFUL + userEntity.name);
        this.userEntity = userEntity;
    }
}
