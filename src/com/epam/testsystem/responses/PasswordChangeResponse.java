package com.epam.testsystem.responses;

import com.epam.testsystem.common.Messages;

public final class PasswordChangeResponse {
    public final String message;

    public PasswordChangeResponse(final boolean isPasswordChanged) {
        message = (isPasswordChanged ? Messages.PASSWORD_CHANGED : Messages.PASSWORD_DONOT_CHANGED);
    }
}
