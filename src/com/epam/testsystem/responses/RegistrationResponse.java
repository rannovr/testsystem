package com.epam.testsystem.responses;

import com.epam.testsystem.common.Messages;

public final class RegistrationResponse {
    public final boolean isRegistered;
    public final String message;

    public RegistrationResponse() {
        message = Messages.REGISTRATION_SUCCESSFUL;
        isRegistered = true;
    }

    public RegistrationResponse(final String eMail) {
        message = Messages.REGISTRATION_UNSUCCESSFUL + " " + eMail + " is already registered!";
        isRegistered = false;
    }
}