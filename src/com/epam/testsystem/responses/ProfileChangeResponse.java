package com.epam.testsystem.responses;

import com.epam.testsystem.common.Messages;

public final class ProfileChangeResponse {
    public final String message;

    public ProfileChangeResponse(final boolean isProfileUpdated) {
        message = (isProfileUpdated ? Messages.PROFILE_CHANGED : Messages.PROFILE_DONOT_CHANGED);
    }
}