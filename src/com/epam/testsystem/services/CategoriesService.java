package com.epam.testsystem.services;

import com.epam.testsystem.dao.CategoriesDAO;
import com.epam.testsystem.entities.CategoryEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public final class CategoriesService extends AbstractService<CategoryEntity> {
    @Autowired
    private CategoriesDAO categoriesDAO;

    public final List<CategoryEntity> getChildrenByParentID(final Long id) {
        return categoriesDAO.getChildrenByParentID(id);
    }

    public final List<CategoryEntity> getFullTreeCategories() {
        final List<CategoryEntity> allCategories = new ArrayList<>();

        for (final CategoryEntity categoryEntity : getChildrenByParentID(0L))
            allCategories.add(createBranchOfTree(categoryEntity));

        return allCategories;
    }

    private CategoryEntity createBranchOfTree(final CategoryEntity category) {
        final List<CategoryEntity> subCategories = getChildrenByParentID(category.id);

        if (subCategories.size() == 0) {
            category.isLeaf = true;
            return category;
        }

        category.nodes = new HashSet<>();
        for (CategoryEntity categoryEntity : subCategories)
            category.nodes.add(createBranchOfTree(categoryEntity));

        return category;
    }
}
