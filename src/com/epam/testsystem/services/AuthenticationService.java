package com.epam.testsystem.services;

import com.epam.testsystem.dao.UsersDAO;
import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.responses.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService extends AbstractService<UserEntity> {
    @Autowired
    UsersDAO usersDAO;

    public final AuthenticationResponse authenticate(final String eMail) {
        try {
            return new AuthenticationResponse(usersDAO.getUserByEmail(eMail));
        } catch (final Exception e) {
            return new AuthenticationResponse();
        }
    }
}
