package com.epam.testsystem.services;

import com.epam.testsystem.dao.TestsDAO;
import com.epam.testsystem.entities.TestEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public final class TestsService extends AbstractService<TestEntity> {
    @Autowired
    private TestsDAO testsDAO;

    public List<TestEntity> getTestsByCategoryID(final Long categoryID) {
        return testsDAO.getTestsByCategoryID(categoryID);
    }

    public TestEntity getByID(final Long testID) {
        return testsDAO.getByID(testID);
    }
}