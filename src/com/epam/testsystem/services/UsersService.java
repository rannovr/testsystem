package com.epam.testsystem.services;

import com.epam.testsystem.enums.UserRoles;
import com.epam.testsystem.dao.UsersDAO;
import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.responses.PasswordChangeResponse;
import com.epam.testsystem.responses.ProfileChangeResponse;
import com.epam.testsystem.responses.RegistrationResponse;
import com.epam.testsystem.utilities.HashUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

@Service
public final class UsersService extends AbstractService<UserEntity> {
    @Autowired
    private UsersDAO usersDAO;

    public final UserEntity getUser(final Long id) {
        return usersDAO.getByID(id);
    }

    public final RegistrationResponse createUser(final String eMail, final String password, final String name, final String surname) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return usersDAO.createUser(eMail, HashUtility.SHA1(password), name, surname, UserRoles.ROLE_USER.getRole());
    }

    public PasswordChangeResponse changeUserPassword(final String newPassword, final String oldPassword) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return usersDAO.changeUserPassword(SecurityContextHolder.getContext().getAuthentication().getName(), HashUtility.SHA1(newPassword), HashUtility.SHA1(oldPassword));
    }

    public final UserEntity getProfile(final Long id) {
        return usersDAO.getProfile(id);
    }

    public final UserEntity getCurrentProfile() throws Exception {
        return usersDAO.getUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public final ProfileChangeResponse changeCurrentProfile(final String name, final String surname) {
        return usersDAO.changeProfile(SecurityContextHolder.getContext().getAuthentication().getName(), name, surname);
    }
}
