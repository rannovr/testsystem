package com.epam.testsystem.services;

import com.epam.testsystem.dao.ExamsDAO;
import com.epam.testsystem.entities.ExamEntity;
import com.epam.testsystem.enums.ExamStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamsService extends AbstractService<ExamEntity> {
    @Autowired
    private ExamsDAO examsDAO;

    public final List<ExamEntity> searchExamByEmailAndStatus(final String eMail, final ExamStatus status) {
        return examsDAO.searchByEmailAndStatus(eMail, status);
    }
}
