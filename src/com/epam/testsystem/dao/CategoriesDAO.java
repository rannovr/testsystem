package com.epam.testsystem.dao;

import com.epam.testsystem.entities.CategoryEntity;
import com.epam.testsystem.mappers.CategoryMapper;
import com.epam.testsystem.special.Arguments;
import com.epam.testsystem.tables.CategoriesTable;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public final class CategoriesDAO extends AbstractDAO<CategoryEntity> {
    private String queryChildrenByParentID;

    public CategoriesDAO() { super(new CategoryMapper()); }

    public final List<CategoryEntity> getChildrenByParentID(final Long parentID) {
        return search(queryChildrenByParentID, new Arguments(CategoriesTable.PARENT_ID, parentID));
    }

    @Required
    public final void setQueryChildrenByParentID(final String queryChildrenByParentID) {
        this.queryChildrenByParentID = queryChildrenByParentID;
    }
}
