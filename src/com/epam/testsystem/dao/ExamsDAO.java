package com.epam.testsystem.dao;

import com.epam.testsystem.entities.ExamEntity;
import com.epam.testsystem.enums.ExamStatus;
import com.epam.testsystem.mappers.ExamMapper;
import com.epam.testsystem.special.Arguments;
import com.epam.testsystem.tables.ExamsTable;
import com.epam.testsystem.tables.UsersTable;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public final class ExamsDAO extends AbstractDAO<ExamEntity> {
    private String querySearchByEMailAndStatus;
    private String querySearchByEmail;

    public ExamsDAO() {
        super(new ExamMapper());
    }

    public final List<ExamEntity> searchByEmailAndStatus(final String eMail, final ExamStatus status) {
        if (status == null)
            return search(querySearchByEmail, new Arguments(UsersTable.USER_EMAIL, eMail));

        final Arguments arguments = new Arguments(UsersTable.USER_EMAIL, eMail);
        arguments.add(ExamsTable.EXAM_STATUS, status.toString());
        return search(querySearchByEMailAndStatus, arguments);
    }

    @Required
    public final void setQuerySearchByEmail(final String querySearchByEmail) {
        this.querySearchByEmail = querySearchByEmail;
    }

    @Required
    public final void setQuerySearchByEmailAndStatus(final String querySearchByEmailAndStatus) {
        this.querySearchByEMailAndStatus = querySearchByEmailAndStatus;
    }
}
