package com.epam.testsystem.dao;


import com.epam.testsystem.entities.TestEntity;
import com.epam.testsystem.mappers.TestMapper;
import com.epam.testsystem.special.Arguments;
import com.epam.testsystem.tables.TestsTable;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

public final class TestsDAO extends AbstractDAO<TestEntity> {
    private String queryTestsByCategoryID;
    private String queryGetByID;

    public TestsDAO() { super(new TestMapper()); }

    public final List<TestEntity> getTestsByCategoryID(final Long categoryID) {
        return search(queryTestsByCategoryID, new Arguments(TestsTable.CATEGORY_ID, categoryID));
    }

    public final TestEntity getByID(final Long testID) {
        return get(queryGetByID, new Arguments(TestsTable.TEST_ID, testID));
    }

    @Required
    public final void setQueryTestsByCategoryID(final String queryTestsByCategoryID) {
        this.queryTestsByCategoryID = queryTestsByCategoryID;
    }

    @Required
    public final void setQueryGetByID(final String queryGetByID) {
        this.queryGetByID = queryGetByID;
    }
}
