package com.epam.testsystem.dao;

import com.epam.testsystem.entities.UserEntity;
import com.epam.testsystem.mappers.UserMapper;
import com.epam.testsystem.responses.AuthenticationResponse;
import com.epam.testsystem.responses.RegistrationResponse;
import com.epam.testsystem.special.Arguments;
import com.epam.testsystem.tables.ChangePasswordTable;
import com.epam.testsystem.tables.UsersTable;
import org.springframework.beans.factory.annotation.Required;
import com.epam.testsystem.responses.PasswordChangeResponse;
import com.epam.testsystem.responses.ProfileChangeResponse;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public final class UsersDAO extends AbstractDAO<UserEntity> {
    private String queryGetByID;
    private String queryCreate;
    private String queryChangePassword;
    private String queryGetProfile;
    private String queryChangeProfile;
    private String queryUserByEmail;

    public UsersDAO() {
        super(new UserMapper());
    }

    public final UserEntity getByID(final Long id) {
        return get(queryGetByID, new Arguments(UsersTable.USER_ID, id));
    }

    public final RegistrationResponse createUser(final String eMail, final String password, final String name, final String surname, final String role) {
        try {
            final Arguments arg = new Arguments(UsersTable.USER_EMAIL, eMail);
            arg.add(UsersTable.USER_PASS, password);
            arg.add(UsersTable.USER_NAME, name);
            arg.add(UsersTable.USER_SURNAME, surname);
            arg.add(UsersTable.USER_ROLE, role);
            insert(queryCreate, arg);
            return new RegistrationResponse();
        } catch (final DuplicateKeyException e) {
            return new RegistrationResponse(eMail);
        }
    }

    public final UserEntity getProfile(final Long id) {
        return get(queryGetProfile, new Arguments(UsersTable.USER_ID, id));
    }

    public final PasswordChangeResponse changeUserPassword(final String eMail, final String newPassword, final String oldPassword) {
        final Arguments arg = new Arguments(ChangePasswordTable.USER_EMAIL, eMail);
        arg.add(ChangePasswordTable.USER_NEW_PASSWORD, newPassword);
        arg.add(ChangePasswordTable.USER_OLD_PASSWORD, oldPassword);
        return new PasswordChangeResponse(update(queryChangePassword, arg) > 0);
    }

    public final ProfileChangeResponse changeProfile(final String email, final String name, final String surname) {
        final Arguments arg = new Arguments(UsersTable.USER_EMAIL, email);
        arg.add(UsersTable.USER_NAME, name);
        arg.add(UsersTable.USER_SURNAME, surname);
        return new ProfileChangeResponse(update(queryChangeProfile, arg) > 0);
    }

    public final UserEntity getUserByEmail(final String eMail) throws Exception {
        return get(queryUserByEmail, new Arguments(UsersTable.USER_EMAIL, eMail));
    }

    @Required
    public final void setQueryGetByID(final String queryGetByID) {
        this.queryGetByID = queryGetByID;
    }

    @Required
    public final void setQueryCreate(final String queryPostUser) {
        this.queryCreate = queryPostUser;
    }
    
    @Required
    public final void setQueryUserByEmail(final String queryUserByEmail) {
        this.queryUserByEmail = queryUserByEmail;
    }
    
    @Required
    public final void setQueryGetProfile(final String queryGetProfile) {
        this.queryGetProfile = queryGetProfile;
    }

    @Required
    public final void setQueryChangePassword(final String queryChangePassword) {
        this.queryChangePassword = queryChangePassword;
    }

    @Required
    public final void setQueryChangeProfile(final String queryChangeProfile) {
        this.queryChangeProfile = queryChangeProfile;
    }
}
