package com.epam.testsystem.entities;

import java.util.Set;

public final class CategoryEntity extends AbstractEntity {
    public Long parentID;
    public boolean isLeaf;
    public String categoryName;
    public Set<CategoryEntity> nodes;
}
