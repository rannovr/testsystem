package com.epam.testsystem.entities;

import java.util.Date;

public final class ExamEntity extends AbstractEntity {
    public Long     userID;
    public Long     testID;
    public String   description;
    public Date     date;
    public String   status;
}
