package com.epam.testsystem.entities;


import java.sql.Time;

public final class TestEntity extends AbstractEntity {
    public String   testName;
    public boolean  isAvailable;
    public Long     categoryID;
    public Time     allocateTime;
}
