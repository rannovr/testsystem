package com.epam.testsystem.entities;

public final class ChangePasswordEntity extends AbstractEntity {
    public String newPassword;
    public String oldPassword;
}
