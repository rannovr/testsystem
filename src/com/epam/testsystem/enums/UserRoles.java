package com.epam.testsystem.enums;

public enum UserRoles {
    ROLE_USER("ROLE_USER"),
    ROLE_TEACHER("ROLE_TEACHER"),
    ROLE_OPERATOR("ROLE_OPERATOR"),
    ROLE_ADMIN("ROLE_ADMIN");

    private final String role;

    UserRoles(String role) {
        this.role = role;
    }

    public final String getRole() {
        return role;
    }
}
