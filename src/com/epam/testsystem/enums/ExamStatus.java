package com.epam.testsystem.enums;

public enum ExamStatus {
    PASSED("PASSED"),
    NOT_PASSED("NOT_PASSED"),
    UNCHECKED("UNCHECKED");

    private final String status;

    ExamStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
